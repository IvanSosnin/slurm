## Ставим ceph с помощью ceph-ansible
> Все работы проводим под рутом на master-1
> С sbox Заходим на master-1

```bash
ssh master-1.s000
sudo -s
```
> Клонируем кубеспрей в каталог /srv
```bash
cd /srv
git clone https://gitlab.slurm.io/slurm3/ceph.git
```
> Устанавливаем ansible и зависимости
```bash
cd ceph
pip install -r requirements.txt
```

> Исправляем инвентарь
```bash
cd inventory

vi hosts
```
> Изменяем s000 на s<номер своего логина> 
> Во всех строчках!!!!
> Изменяем третий октет ip адреса в переменных ansible_host и в ip на номер своего логина
```bash
cd group_vars

vi all.yml
```
> Исправляем
> public_network: "172.21.0.0/24"  на "172.21.<номер своего логина>.0/24"
> cluster_network: "172.21.0.0/24"  на "172.21.<номер своего логина>.0/24"

> Возвращаемся в корень репо
```bash
cd ../..
```

> Запускаем сценарий
> например  sh  _deploy_cluster.sh student001

```bash
sh  _deploy_cluster.sh <свой логин>
```
> Следим за выводом на экран. ждем отчета об успешном завершении
> Если при завершении есть красные надписи fail= смотрим вывод, ищем ошибки в инвентаре.

> Если все хорошо последний таск сценария покажет в выводе:
`health: HEALTH_OK`

> Далее будем работать на двух серверах
> node-1 и master-1
> С sbox Заходим на node-1

```bash
ssh node-1.s000
sudo -s
```
> Проверяем что цеф живой

```bash
ceph health
```

## Создаем пул в цефе для RBD дисков
> делаем на node-1

```bash
ceph osd pool create kube 32
ceph osd pool application enable kube kubernetes
```
> Создаем пользователя в ceph, с правами записи в пул kube

```bash
ceph auth get-or-create client.user mon 'allow r, allow command "osd blacklist"' osd 'allow rwx pool=kube'
```
> создаем секрет с ключом админа в кубе
> смотрим ключ админа в цеф
> node-1
```bash
ceph auth get-key client.admin
```

> вывод:
 AQB8x9Jbk8RFKhAAfNVrIka3hlktspJUV9tnlw==

> выполняем на master-1, подставляем значение ключа в создание секрета
> Если секрет был уже создан ранее и вы хотите его изменить. Самый простой способ удалить и создать заново

```bash
# kubectl delete secret ceph-secret --namespace=kube-system
kubectl create secret generic ceph-secret --type="kubernetes.io/rbd" --from-literal=key='xxxxxxxxxxxxxxxxxxxx==' --namespace=kube-system
```
>
> Создаем пользователя в ceph, с правами записи в пул kube
> Возвращаемся на node-1

```bash
ceph auth get-key client.user
```

> вывод:
AQCO9NJbhYipKRAAMqZsnqqS/T8OYQX20xIa9A==

> выполняем на master-1, подставляем значение ключа в создание секрета
> Если секрет был уже создан ранее и вы хотите его изменить. Самый простой способ удалить и создать заново

```bash
# kubectl delete secret ceph-secret-user --namespace=default
kubectl create secret generic ceph-secret-user --type="kubernetes.io/rbd" --from-literal=key='yyyyyyyyyyyyyyyyyyyyyyyyyyy==' --namespace=default
```

> создаем StorageClass
>Редактируем файл sc.yml, в этом файле правим ip адреса мониторов, третий октет на номер своего студента.
>В поле userId: я уже указал имя пользователя user

> Команды с kubectl delete удаляют сучществующий объект, чтобы можно было создать новый
> Аналогично с изменением storageclass - удаляем и создаем заново
> master-1

```bash
# kubectl delete sc kube
kubectl apply -f sc.yml
```

> Проверяем что создался sc
```bash
kubectl get sc
```
>
> Создаем pvc
> в файле pvc.yml лежит манифест для PersistenceVolumeClaim
> на master-1

```bash
kubectl apply -f pvc.yml --namespace=default
```
> команды диагностики:
> запускать на master-1

> посмотреть список созданных PersistenceVolume
```bash
kubectl get pv
```
> посмотреть на ошибки создания тома
```bash
kubectl describe pvc prometheus-3gb --namespace=default
```

> Самая основная проблема была такая:
>в describe pvc было вот такое написано:
>  Warning    ProvisioningFailed  3s                    persistentvolume-controller  Failed to provision volume with StorageClass "kube": failed to get admin secret from ["kube-system"/"ceph-secret"]: failed to get secret from ["kube-system"/"ceph-secret"]

>Помогло удаление секрета админа и создание его заново.

> остальные ошибки были из-за проблем с указанием не тех ip адресов мониторам или не тех username для admin и user

## Создание пула для Cephfs не требуется. оба пула были созданы сценарием ceph-ansible
