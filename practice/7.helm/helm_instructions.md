# Практика по использованию helm-а.

Состоит из двух частей. Каждая часть выполняется путём запуска команд или
применения (с помощью kubectl apply -f) манифестов.

## Часть 1. Локальный Helm/Tiller

Установка tiller в отдельное пространство имён с правами
непривилегированного сервисного аккаунта (создаюётся YAML-манифестом)
и деплой приложения-примера fileshare. Чарт примера лежит тут же.

### Шаг 0.

Заходим в каталог с чартом

```bash
# cd 1-namespaced
```

### Шаг 1. Подготовка кластера

Создаём и применяем манифест (здесь *00_prepare.yaml*) для создания пространства
имён, сервисного аккаунта, роли и ролебиндинга

```bash
cat > 00_prepare.yaml <<EOF
---
# kubectl create serviceaccount tiller --namespace kube-system
apiVersion: v1
kind: Namespace
metadata:
  name: fileshare
...
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: fileshare
  namespace: fileshare
...
---
# kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: fileshare
  namespace: fileshare
rules:
- apiGroups:
  - ""
  - extensions
  - apps
  - certmanager.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
...
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: fileshare
  namespace: fileshare
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: fileshare
subjects:
- kind: ServiceAccount
  name: fileshare
  namespace: fileshare
...
EOF
# kubectl apply -f 00_prepare.yaml
```
Обратите внимание, что роли/аккаунту fileshare мы дали полные права на группу API certmanager.k8s.io

### Шаг 2. Подготовка локальной машины и tiller-а

Установим пакет helm, проинициализирует tiller в пространстве имён
fileshare.

```bash
# yum -y install helm
# helm init --service-account fileshare --tiller-namespace fileshare --history-max 10
```

### Шаг 3. 

Правим файл values.yaml заменяя XXX на номер своего студента. Устанавливаем локального чарт fileshare с параметрами из файла values.yaml.

```bash
# helm upgrade --install --wait fileshare --tiller-namespace fileshare --namespace fileshare --values values.yaml fileshare
```

### Шаг 4. Проверка результата

Через небольшое время проверяем результат деплоя командой *helm
status* и работу приложения в кластере curl-ом (меняя XXX на свой номер).

```bash
# helm status --tiller-namespace fileshare fileshare
# curl -s http://fileshare.sXXX.slurm.io/
# curl -s http://fileshare.sXXX.slurm.io/files/
# curl -s http://fileshare.sXXX.slurm.io/files/ -T values.yaml
# curl -s http://fileshare.sXXX.slurm.io/files/
```

последние две команды загружают ваш файл values.yaml на вашу файлшару
в кластере и проверяют, что он действительно загрузился (должен быть в
распечатке каталога).

## Часть 2. Глобальный Helm/Tiller

Системная установка tiller в пространство имён kube-system с полными
привилегиями (сервисный аккаунт, роль и кластеррольбиндинг создаются
командами kubectl). Установка чарта *cephfs-provisioner* для всего
кластера. Чарт лежит в удалённом репозитории.

### Шаг 0.

Заходим в каталог с чартом

```bash
# cd ../2-system
```

### Шаг 1. Подготовка кластера

Создаём сервис-аккаунт в пространстве имён kube-system и даём ему
роль кластерного администратора (создаём рольбиндинг).

```bash
# kubectl create serviceaccount tiller --namespace kube-system
# kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
```

### Шаг 2. Установка helm/tiller

Установим тиллер с созданым сервис-аккаунтом в пространство имён
kube-system. Добавим удаленный репозиторий чартов и заберём его индекс.

```bash
# helm init --service-account tiller --tiller-namespace kube-system --history-max 10
# helm repo add sb https://charts.southbridge.ru
# helm repo update
```

### Шаг 3. Установка cephfs-provisioner

Скачаем файл values.yaml чарта из удалённого репо и поправим его под
себя. Нужно внести в YAML-список monitors все IP-адреса и порты
mon-серверов ceph.

```bash
# helm inspect values sb/cephfs-provisioner | tee values.yaml
# vim values.yaml
```

в итоге список monitors должен выглядеть примерно так (подставьте свой
номер логина в третьем октете):
```yaml
monitors:
  - 172.21.0.5:6789
  - 172.21.0.6:6789
  - 172.21.0.7:6789
```

также в файле values.yaml видим имя секрета, где провиженер ожидает
увидеть админский ключ ceph-а: *ceph-secret-admin*. Создадим его.
Вместо XXX надо подставить вывод команды "ceph auth get-key
client.admin" на административном узле кластера ceph (*node-1*).

```bash
# kubectl create secret generic ceph-secret-admin --namespace=kube-system --from-literal=secret=XXX
```

Теперь можно собственно поставить чарт.

```bash
# helm upgrade --install --wait cephfs-provisioner --namespace=kube-system --values values.yaml sb/cephfs-provisioner
```

### Шаг 4. Проверка результата

Через небольшое время проверяем результат деплоя.

```bash
# helm status cephfs-provisioner
```
